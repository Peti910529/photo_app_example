package com.example.photoapp.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required

open class Images (
    @PrimaryKey
    var id: Int = 0
): RealmObject()