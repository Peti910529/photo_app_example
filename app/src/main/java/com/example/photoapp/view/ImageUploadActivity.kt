package com.example.photoapp.view

import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.photoapp.R
import com.example.photoapp.model.Images
import kotlinx.android.synthetic.main.activity_image_upload.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import kotlin.Exception

class ImageUploadActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_upload)

        val imageData = Uri.parse(intent.getStringExtra("imageData"))
        imagePreview.setImageURI(imageData)

        button.setOnClickListener{
            val stream = FileOutputStream(File(it.context.filesDir, "image.jpg"))
            try {
                stream.write(imageData?.path?.toByteArray())
                stream.flush()

                Toast.makeText(this, R.string.image_save_succeed, Toast.LENGTH_SHORT).show()
            } catch (e: IOException) {
                Toast.makeText(this, R.string.image_save_failed, Toast.LENGTH_SHORT).show()
            } catch (e: Exception) {
                Toast.makeText(this, R.string.exception_message, Toast.LENGTH_SHORT).show()
            } finally {
                stream.close()
            }
        }
    }
}